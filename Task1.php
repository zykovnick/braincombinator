<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 07.06.2016
 * Time: 18:04
 */

/**
 * Class Init
 */

final class Init{

    private $connection;
    private $resultTypes = ['normal', 'illegal', 'failed', 'success'];

    /**
     * Initiates DB connection
     * Init constructor.
     */
    public function __construct(){
        $this->connection = DBConnection::getInstance()->getConnection();
        $this->create();
        $this->fill();
    }

    /**
     * Creates table test
     * @return bool returns true if successful
     * @throws Exception in a case with problems with DB
     */
    private function create(){
        try {
            $this->connection->query("
                        CREATE TABLE IF NOT EXISTS `test` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `script_name` char(25) NOT NULL,
                          `start_time` int(11) NOT NULL,
                          `end_time` int(11) NOT NULL,
                          `result` enum('normal','illegal','failed','success') NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");

            return true;
        } catch (PDOException $e){
            throw new Exception("DB error: " . $e->getMessage());
        }
    }

    /**
     * Execute data filling to db with test data
     * @return bool returns true if successful
     * @throws Exception in a case with problems with DB
     * @todo multiple insertion
     */
    private function fill(){
        $iters = rand(10, 20);

        for ($i = 1; $i <= $iters; $i++) {

            $data = $this->dataGenerator();

            try {

                $sql = "INSERT INTO `test` (`script_name`, `start_time`, `end_time`, `result`) VALUES (:script_name, :start_time, :end_time, :result)";

                $statement = $this->connection->prepare($sql);
                $statement->execute([
                    'script_name'   =>  $data['script_name'],
                    'start_time'    =>  $data['start_time'],
                    'end_time'      =>  $data['end_time'],
                    'result'        =>  $data['result']
                ]);

                return true;

            } catch (PDOException $e){
                throw new Exception("DB error: " . $e->getMessage());
            }

        }
    }

    /**
     * Generate test row
     * @return array
     */
    private function dataGenerator(){
        return [
            'script_name'   => substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, rand(5, 7)).".txt",
            'start_time'    => time() - rand(1000, 2000),
            'end_time'      => time() + rand(1000, 2000),
            'result'        => $this->resultTypes[array_rand($this->resultTypes, 1)]
        ];
    }

    /**
     * Lists data from from table test
     * @throws Exception in a case with problems with DB
     */
    public function get(){
        try {
            $data = $this->connection->prepare("SELECT * FROM  `test` WHERE  `result` IN ( 'normal',  'success')");
            $data->execute();
        } catch (PDOException $e){
            throw new Exception("DB error: " . $e->getMessage());
        }

        echo "ID\t|\tScript Name\t|\tStart time\t\t|\tEnd time\t\t|\tResult\n";
        while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
            echo  $row['id']."\t|\t". $row['script_name']."\t|\t".date("d.m.Y H:i:s", $row['start_time'])."\t|\t".date("d.m.Y H:i:s", $row['end_time'])."\t|\t".$row['result']."\n";
        }
    }


}

/**
 * Class DBConnection
 * singleton connection to db
 */
class DBConnection{

    protected $db;
    private static $instance;

    private function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=brain', 'root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            throw new Exception("DB error: " . $e->getMessage());
        }

    }

    public static function getInstance(){
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->db;
    }

}

$obj = new Init();
$obj->get();

