<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 07.06.2016
 * Time: 19:40
 */

/**
 * Class FileSeeker
 */
class FileSeeker{

    private $dir;
    private $files = [];

    /**
     * FileSeeker constructor.
     * @param $dir directory for parsing
     */
    public function __construct($dir){
        $this->dir = $dir;
        $this->seek();
        sort($this->files);
        $this->display();
    }

    /**
     * Adding filenames to array
     */
    private function seek(){
        foreach (new DirectoryIterator($this->dir) as $fileInfo) {
            if($fileInfo->isDot()) continue;
            if($fileInfo->getExtension() != 'ixt' and $this->checkName($fileInfo->getFilename())) continue;


            $this->files[] = $fileInfo->getFilename();
        }
    }

    /**
     *
     */
    public function display(){
        for ($i = 0; $i < count($this->files); $i++) {
            echo $this->files[$i]."\n";
        }
    }

    /**
     * Checks is name valid (only digits letters and dot)
     * @param $name name of file of
     * @return bool
     */
    private function checkName($name){
        return preg_match("/^[a-z0-9.]+$/i", $name);
    }

}


$obj = new FileSeeker('datafiles');